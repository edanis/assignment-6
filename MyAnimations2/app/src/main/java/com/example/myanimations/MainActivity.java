package com.example.myanimations;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private AnimationDrawable birdAnimation;
    private ImageView birdImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        birdImage = findViewById(R.id.birdImgid);
        birdImage.setBackgroundResource(R.drawable.bird_anim);
        birdAnimation = (AnimationDrawable) birdImage.getBackground();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.fade_in_animation);
                birdAnimation.start();
                birdImage.startAnimation(startAnimation);
            }
        },50);
        return super.onTouchEvent(event);
    }
}
