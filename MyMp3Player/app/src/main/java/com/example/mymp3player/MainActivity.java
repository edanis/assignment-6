package com.example.mymp3player;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;
    private Button playButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = new MediaPlayer();
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.hold_up);

        playButton = findViewById(R.id.playID);
        playButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(mediaPlayer.isPlaying()){
                    PauseMusic();
                }
                else{
                    StartMusic();
                }
            }
        });
    }

    public void PauseMusic(){
        if(mediaPlayer != null){
            mediaPlayer.pause();
            playButton.setText("Play");
            CurTimeToast();
        }
    }

    public void StartMusic(){
        if(mediaPlayer != null){
            mediaPlayer.start();
            playButton.setText("Pause");
            CurTimeToast();
        }
    }

    @Override
    protected void onDestroy() {
        if(mediaPlayer != null && mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }

    public void CurTimeToast(){
        if(mediaPlayer != null){
            int duration = mediaPlayer.getCurrentPosition();
            String mDuration = String.valueOf(duration);
            Toast.makeText(getApplicationContext(),"duration " + mDuration, Toast.LENGTH_LONG).show();
        }
    }
}
